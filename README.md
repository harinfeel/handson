# S3, CloudFront 웹 호스팅 설정 실습

## Overview

Amazon S3 (Simple Storage Service)는 웹의 어느 곳에서나 언제든지 모든 데이터를 저장하고 검색하는 데 사용할 수 있는 간단한 웹 서비스 인터페이스를 제공합니다.  
이 실습은 S3와 상호 작용하여 객체를 저장, 보기, 이동 및 삭제하는 방법을 보여주기 위해 작성되었습니다.

## Repository

본 실습을 진행하기 위해 하단의 주소에서 파일을 다운로드 합니다.  
다운받은 파일의 압축을 해제하고, 진행되는 실습에서 `S3 버킷 생성후 업로드`를 할 것입니다.

[cf_lab1.tar.gz](https://harintest.s3.ap-northeast-2.amazonaws.com/cf_lab1.tar.gz)

## S3 버킷 생성

S3의 모든 객체는 버킷에 저장됩니다. Amazon S3에 데이터를 저장혀려면 먼저 버킷을 생성해야 합니다.  
다음 과정을 통해 버킷을 생성합니다.

> Note : 버킷 생성 비용은 청구되지 않습니다. 버킷에 객체를 저장하고 버킷 객체를 전송하는 경우에만 요금이 부과됩니다.

**버킷 생성**

1. `AWS Management Console` 로그인
2. 우측 상단의 리전 선택 `아시아 태평양 (서울) ap-northeast-2`
3. `AWS Management Console` 좌측 상단의 `서비스`를 클릭하고 `S3` 페이지로 이동
4. `버킷 만들기` 클릭

**버킷 생성시 주의사항**

1. 버킷의 이름은 Amazon S3에 있는 어떤 기존 버킷 이름과도 중복되지 않아야 합니다.  
2. 이름에 대문자를 사용 할 수 없으며 소문자나 숫자로 시작해야 합니다.  
3. 이름은 3~63자 이내로 작성합니다.  
4. 버킷 생성후 버킷 이름 변경을 할 수 없습니다.

버킷 만들기 대화 상자가 나타나면 다음과 같이 입력합니다.

**`일반구성`**

| Key | Value | description |
|-----|--------|----|
| 버킷 이름 | your-bucket-name | 사용할 버킷 이름 |
| 리전 | 아시아 태평양(서울) ap-northeast-2| 아시아 리전 선택 |

그 외의 다른 사항들은 추후 변경이 가능하므로 일반구성만 세팅하고 진행합니다.  
`버킷 만들기`를 클릭합니다. 버킷이 생성되었습니다!

## S3 파일 업로드

앞선 내용에서 다운로드 받아 압축 해제한 폴더를 S3에 업로드 할 것입니다.

1. 생성한 `버킷 이름 클릭`
2. `업로드` 클릭
3. `폴더 추가` 클릭
4. 압축을 해제한 `cf_lab1 폴더 선택`
5. 파일 업로드에 관한 `팝업창의 업로드 클릭`
6. 제일 하단 `업로드 클릭`

업로드가 성공하면 상태를 한번 확인후 우측 `종료` 버튼을 클릭합니다.

## S3 버킷 권한 설정

버킷을 생성한 후 권한이 설정되어 있지 않아 버킷을 만든 사용자만 사용 가능한 상태입니다.  
정책을 적용하여 S3에 업로드된 객체에 모든 사용자가 읽기 가능하게 적용할 것 입니다.

1. **`버킷개요`** 의 `Amazon 리소스 이름(ARN)`의 ARN을 클립보드에 복사합니다.  
Ex) arn:aws:s3:::sample_bukcet
2. `권한` 클릭
3. **`퍼블릭 액세스 차단(버킷 설정)`** 의 `편집 클릭`
4. `모든 퍼블릭 액세스 차단`의 체크 박스 해제
5. **`새 ACL(액세스 제어 목록)을 통해 부여된 버킷 및 객체에 대한 퍼블릭 액세스 차단`** 의 체크박스 활성화
6. **`임의의 ACL(액세스 제어 목록)을 통해 부여된 버킷 및 객체에 대한 퍼블릭 액세스 차단`** 의 체크 박스 활성화
7. `변경사항 저장` 클릭 후 체크박스에 `확인` 입력
8. 하단의 **`버킷 정책`** 의 `편집` 클릭
9. 하단의 정책을 입력 (**`your-arn`** 부분을 삭제하고 복사한 ARN을 넣습니다.)
   ```
   {
     "Version":"2012-10-17",
     "Statement":[
       {
         "Effect":"Allow",
         "Principal": "*",
         "Action":["s3:GetObject"],
         "Resource":["your-arn/cf_lab1/*"]
       }
     ]
   }
   ```
10. `변경 사항 저장` 클릭

## 웹 페이지 접근 확인

다음과 같은 순서로 웹 페이지를 띄웁니다.

1. 화면에서 보이는 탭에서 `객체` 클릭 후 하단의 `cf_lab1/` 폴더를 클릭
2. 하단 리스트의 `cf_lab1.html` 클릭
3. 화면의 `객체 URL`을 복사하고 새브라우저로 해당 링크를 띄웁니다.

브라우저 화면에 웹페이지가 뜬다면 성공입니다!

## CloudFront 웹 배포 만들기

CloudFront 를 이용하여 웹 페이지 배포를 웹 페이지를 구성합니다.

1. 좌측상단 `서비스` 클릭후 `CloudFront`를 검색하여 이동합니다.
2. `Create Distribution` 클릭
3. **`Web`** 섹션의 `Get Started` 클릭
4. `Origin Domain Name` 의 필드를 클릭하여 앞서 생성한 S3 버킷을 선택
5. `Origin Path` 에 `/cf_lab1` 입력
6. **`Default Cache Behavior Settings`** 기본값 적용
7. **`Distribution Settings`** 기본값 적용
8. `Create Distribution` 클릭

대략 15분 이내에 클라우드 프론트 배포가 완료됩니다.  
Status가 Deployed 가 되면 완료된 것입니다.

화면에서 `Domain Name`을 확인하고 테스트를 진행합니다.

1. CloudFront `Domain Name`을 복사 ( `예시` : d3e8c6cbxepozk.cloudfront.net)
2. `Domain Name`을 브라우저에 붙여넣고 `/cf_lab1.html` 을 도메인 이름 끝에 추가     
   ```html
   http://d3e8c6cbxepozk.cloudfront.net/cf_lab1.html
   ```

CloudFront를 사용하지 않고 보유중인 도메인 (`예시` : www.exam.com )을 사용하려면 CNAME을 설정해야 합니다.  
본 실습에서는 도메인을 사용하지 않으며 CNAME 설정은 문서를 참조합니다.

`참조`
```html
http://docs.aws.amazon.com/ko_kr/AmazonCloudFront/latest/DeveloperGuide/CNAMEs.html
```

## 콘텐츠 업데이트

CloudFront의 모든 Pop 위치에 콘텐츠가 최대 24시간 동안 캐시됩니다.  
캐시된 내용을 변경하는 실습을 진행합니다.

### 객체 업데이트

**`파일 버전 관리`**  

CloudFront에서 객체 업데이트의 가장 간단한 방법은 파일 버전관리를 사용하는 것입니다.  
먼저 S3의 버전관리를 사용하도록 설정합니다.

1. `서비스` -> `S3` -> `생성한 버킷` 의 순서대로 이동
2. 메인 섹션에서 `속성` -> `버킷 버전 관리` 섹션의 `편집`을 선택
3. 버킷 버전관리 `활성화` 체크 후 `변경 사항 저장`

버전관리가 활성화 되었으며 `cf_lab1.html` 파일의 내용을 하단과 같이 변경하여 새로 업로드 후 S3에서 제공하는 URL을 확인합니다. 

```diff
-<p> <img src="grand_canyon.jpg" alt="my test image" width="1024" />
+<p> <img src="grand_canyon_v2.jpg" alt="my test image" width="1024" />
 ```

파일업로드후 `cf_lab1.html` 파일을 클릭하여 `버전` 섹션을 확인해보면 버전관리가 활성화 된 것을 확인 할 수 있습니다.  
`객체 URL`을 복사하여 새 브라우저 창에 페이지를 로드하면 이전에 확인한 `cf_lab1.html` 페이지가 바뀐것을 확인 할 수 있습니다.

### 객체 교체 또는 삭제

파일 버전 관리가 적절한 솔루션이 아닐 경우 S3에서 파일을 제거하거나 교체해도 문제가 즉시 해결되지는 않습니다.  
다음을 통해 확인해 보겠습니다.

   ```http
   # CloudFront
   http://d3e8c6cbxepozk.cloudfront.net/cf_lab1.html
   
   # S3
   https://harintest.s3.ap-northeast-2.amazonaws.com/cf_lab1/cf_lab1.html
   ```

`CloudFront에서 제공하는 URL과 S3의 URL`을 확인하여 양쪽페이지를 비교해보면 `이미지가 다른것을 확인` 할 수 있습니다.  
`S3`에서는 `현재 업로드된 이미지를 반영`하여 보여주지만 `CloudFront`에서는 이전에 `캐싱된 이미지를 24시간 동안 보여주기 때문`입니다.  
`CloudFRont` 캐시서버에 있는 `cf_lab1.html` 을 강제로 제거하려면 기존 복사본을 무효화 해야합니다.  

1. `AWS Management Console` -> `서비스` -> `CloudFront`로 이동
2. 이 실습에 대해 만든 배포판을 선택
3. `Distribution Settings (배포설정)` 클릭
4. `Invalidation` 탭을 클릭
5. `Create Invalidation`을 클릭
6. 무효화 할 파일 이름을 입력 `cf_lab1.html`
7. `Invalidate` 클릭

무효화 상태가 진행 -> 완료로 변경 될 때까지 기다립니다. 몇 분 소요됩니다.

무효화가 완료되면 `CloudFront`의 URL 페이지를 띄워놓은 브라우저를 새로고침 합니다.  
이제 웹 페이지의 변경된 페이지가 S3와 동일한 이미지로 바뀌어 있는것을 확인 할 수 있습니다.  
페이지를 새로 고치면 `CloudFront`가 캐시된 파일이 없으므로 `S3` 에서 업데이트 된 파일을 가져와 새 버전을 캐시로 복사하게 됩니다.

## 실습종료

본 실습은 비용이 발생함으로 실습을 마무리 한 후 모든 리소스를 제거합니다.

### CloudFront Resource 제거

1. `서비스` -> `CloudFront` 이동
2. 생성한 배포버전 체크 박스 선택 후 `Diable` 클릭 
3. `Status`가 `진행` -> `완료`로 변경 (15분 이내에 완료)
4. 배포버전 체크 박스 선택 후 `Delete` 클릭

### S3 Resource 제거

1. `서비스` -> `S3` -> `생성한 버킷`의 라디오 박스 선택
2. `삭제` 클릭
3. 경고 문구의 `버킷 비우기 구성` 클릭
4. 영구 삭제 진행

